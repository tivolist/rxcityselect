package ru.fotostrana.rxcityselect.models.FS;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Алексей on 23.07.2015.
 */
public class City {
    public String name;

    @SerializedName("city_id")
    public int cityId;

    public String country;

    @SerializedName("country_id")
    public int countryId;

    @SerializedName("description")
    public String regionDescription = "";
}
