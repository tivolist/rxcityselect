package ru.fotostrana.rxcityselect.models.CitySearch;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.fotostrana.rxcityselect.models.FS.City;

/**
 * Created by Алексей on 24.07.2015.
 */
public class CitySearchResult {
    @SerializedName("result")
    public List<City> result;
}
