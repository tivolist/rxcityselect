package ru.fotostrana.rxcityselect.models.CitySearch;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Алексей on 24.07.2015.
 */
public class CitySearchResults {
    @SerializedName("result")
    public CitySearchResult result;
}
