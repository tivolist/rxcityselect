package ru.fotostrana.rxcityselect.api;

import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.fotostrana.rxcityselect.models.CitySearch.CitySearchResults;
import rx.Observable;

/**
 * Created by Алексей on 29.02.2016.
 */
public interface  FsAPI {
    @GET("/oapi/method/rating.getCityByName") Observable<CitySearchResults> searchCity(
            @Query("name") String name,
            @Query("access_token") String accessToken,
            @Query("v") String v, // OAPI_VERSION = "1.0.3.40"
            @Query("isdbg") String isDbg, // 1
            @Query("vendor") String vendor // VENDOR_ID = 2
            );
}
