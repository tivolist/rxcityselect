package ru.fotostrana.rxcityselect.ui.activities;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.gson.Gson;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.fotostrana.rxcityselect.R;
import ru.fotostrana.rxcityselect.api.FsAPI;
import ru.fotostrana.rxcityselect.models.FS.City;
import ru.fotostrana.rxcityselect.ui.adapters.CitiesListAdapter;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

public class MainActivity extends BaseActivity {
    public static final int THRESHOLD = 3;
    public static final int MAX_CITIES = 10;
    public static final int AUTOCOMPLETE_DELAY = 700;

    public static final String OAPI_URL = "https://fotostrana.ru/oapi/method/";
    public static final String OAPI_TOKEN = "yAq9AQkSfyjeBMPGCqsq9uyuFua1ht6ZwPYW";
    public static final String OAPI_VERSION = "1.0.3.40";
    public static final String OAPI_IS_DBG = "1";
    public static final String OAPI_VENDOR = "2";

    @Bind(R.id.searchText) EditText searchText;
    @Bind(R.id.progressBar) ProgressBar progressBar;
    @Bind(R.id.listView) ListView listView;

    public FsAPI mFsAPI;
    public Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.plant(new Timber.DebugTree());
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        gson = new Gson();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(OAPI_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        mFsAPI = retrofit.create(FsAPI.class);
    }

    @Override protected void onStart() {
        super.onStart();
        unSubscribeOnStop(searchTextChanged()
                .debounce(AUTOCOMPLETE_DELAY, TimeUnit.MILLISECONDS)
                .flatMap(s1 -> {
                    if (s1.length() > 0) {
                        return Observable.just(s1)
                                .filter(s2 -> s2.length() >= THRESHOLD)
                                .flatMap(s2 -> mFsAPI.searchCity(s2, OAPI_TOKEN, OAPI_VERSION, OAPI_IS_DBG, OAPI_VENDOR))
                                .map(citySearchResults -> citySearchResults.result.result);
                    } else {
                        return Observable.just(Collections.emptyList());
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> {
                    if (!list.isEmpty()) {
                        listView.setAdapter(new CitiesListAdapter(MainActivity.this, (List<City>) list));
                    } else {
                        listView.setVisibility(View.GONE);
                    }
                })
        );
    }

    public Observable<String> searchTextChanged() {
        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override public void call(Subscriber<? super String> subscriber) {
                searchText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override public void afterTextChanged(Editable s) {
                        if (listView.getVisibility() == View.GONE){
                            listView.setVisibility(View.VISIBLE);
                        }
                        if (s.toString().length() < THRESHOLD){
                            listView.setVisibility(View.GONE);
                        }
                        if (!subscriber.isUnsubscribed()) {
                            subscriber.onNext(s.toString());
                        }
                    }
                });
            }
        });
    }

    @OnItemClick(R.id.listView) void onCityClick(int position) {
        searchText.setText(((City) listView.getAdapter().getItem(position)).name);
        listView.setVisibility(View.GONE);
    }

}
