package ru.fotostrana.rxcityselect.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.fotostrana.rxcityselect.R;
import ru.fotostrana.rxcityselect.models.FS.City;

/**
 * Created by Алексей on 29.02.2016.
 */
public class CitiesListAdapter extends BaseAdapter {

    private final Context mContext;
    public List<City> mCityList;

    public CitiesListAdapter(Context context, List<City> cityList) {
        mContext = context;
        mCityList = cityList;
    }

    @Override public int getCount() {
        return mCityList.size();
    }

    @Override public City getItem(int position) {
        return mCityList.get(position);
    }

    @Override public long getItemId(int position) {
        return position;
    }

    @Override public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.dropdown_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        City city = getItem(position);
        holder.city.setText(city.name);
        if ("".equals(city.regionDescription)) {
            holder.region.setVisibility(View.GONE);
            holder.region.setText("");
        } else {
            holder.region.setVisibility(View.VISIBLE);
            holder.region.setText(city.regionDescription);
        }

        return convertView;
    }

    static class ViewHolder {
        @Bind(R.id.city) TextView city;
        @Bind(R.id.region) TextView region;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


}
