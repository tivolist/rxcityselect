package ru.fotostrana.rxcityselect.ui.activities;

import android.support.v7.app.AppCompatActivity;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Алексей on 02.03.2016.
 */
public class BaseActivity  extends AppCompatActivity {
    CompositeSubscription mCompositeSubscription;

    public void unSubscribeOnStop(Subscription subscription) {
        if (mCompositeSubscription != null) {
            mCompositeSubscription.add(subscription);
        }
    }

    @Override protected void onStart() {
        super.onStart();
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override protected void onStop() {
        super.onStop();
        if (mCompositeSubscription != null) {
            mCompositeSubscription.unsubscribe();
        }
    }
}
